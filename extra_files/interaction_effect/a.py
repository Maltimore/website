import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score



fun = lambda x: x[:, 0] + x[:, 1]
mesh_x1, mesh_x2 = np.meshgrid(np.linspace(0, 1, 100), np.linspace(0, 1, 100))

# Reshape the meshgrid to shape (n_samples, 2)
X_mesh = np.column_stack([mesh_x1.ravel(), mesh_x2.ravel()])

# Apply the function to the reshaped meshgrid
z = fun(X_mesh)

# Reshape z back to the grid shape
z = z.reshape(mesh_x1.shape)

# Plot a heatmap of the values
plt.imshow(z, extent=[0, 1, 0, 1], origin='lower', cmap='viridis')
plt.colorbar(label='f()')
plt.xlabel('x1')
plt.ylabel('x2')

X = np.random.uniform(low=(0., 0.), high=(1., 1.), size=(500, 2))
y = fun(X)
plt.scatter(X[:, 0], X[:, 1], color='black', s=1, marker='o')
plt.savefig('heatmap_with_scatter_all.png')
plt.close()

model = LinearRegression()
model.fit(X[:, 0].reshape(-1, 1), y)
x1_reg_line = np.linspace(0, 1, 10).reshape(10, 1)
y_hat_reg_line = model.predict(x1_reg_line)
y_hat = model.predict(X[:, 0].reshape(-1, 1))
r2_score_ = round(r2_score(y, y_hat), 3)
plt.figure()
plt.scatter(X[:, 0], y, color='black', s=1, marker='o')
plt.plot(x1_reg_line, y_hat_reg_line)
plt.xlabel('x1')
plt.ylabel('y')
plt.suptitle(f'r2_score: {r2_score_}')
plt.savefig('regression_x1_y_all.png')
plt.close()

mask = np.abs(fun(X) - 1) < 0.1
X = X[mask]
y = y[mask]
plt.figure()
plt.scatter(X[:, 0], X[:, 1], color='black', s=1, marker='o')
plt.imshow(z, extent=[0, 1, 0, 1], origin='lower', cmap='viridis')
plt.colorbar(label='f()')
plt.savefig('heatmap_with_scatter_masked.png')
plt.close()

model = LinearRegression()
model.fit(X[:, 0].reshape(-1, 1), y)
x1_reg_line = np.linspace(0, 1, 10).reshape(10, 1)
y_hat_reg_line = model.predict(x1_reg_line)
y_hat = model.predict(X[:, 0].reshape(-1, 1))
r2_score_ = round(r2_score(y, y_hat), 3)
plt.figure()
plt.scatter(X[:, 0], y, color='black', s=1, marker='o')
plt.plot(x1_reg_line, y_hat_reg_line)
plt.xlabel('x1')
plt.ylabel('y')
plt.suptitle(f'r2_score: {r2_score_}')
plt.savefig('regression_x1_y_masked.png')
plt.close()
